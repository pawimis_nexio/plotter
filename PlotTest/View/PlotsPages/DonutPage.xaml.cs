﻿using System;
using System.Collections.Generic;
using PlotTest.Plots;
using PlotTest.Service;
using PlotTest.View.Base;
using Xamarin.Forms;

namespace PlotTest.View.PlotsPages
{
    public partial class DonutPage : BaseContentPage
    {
        public DonutPage()
        {
            InitializeComponent();
            var data = new MockPlotData();
            var barChart = new DonutChart() { Entries = data.DataPoints };
            this.chart.Chart = barChart;
        }
    }
}
