﻿using System;
using System.Collections.Generic;
using PlotTest.Plots;
using PlotTest.Service;
using PlotTest.View.Base;
using Xamarin.Forms;

namespace PlotTest.View.PlotsPages
{
    public partial class RadarPage : BaseContentPage
    {
        public RadarPage()
        {
            InitializeComponent();
            var data = new MockPlotData();

            var barChart = new RadarChart() { Entries = data.DataPoints };
            this.chart.Chart = barChart;
        }
    }
}
