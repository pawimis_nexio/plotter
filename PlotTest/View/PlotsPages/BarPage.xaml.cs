﻿using System;
using System.Collections.Generic;
using PlotTest.Plots;
using PlotTest.Service;
using PlotTest.View.Base;
using Xamarin.Forms;

namespace PlotTest.View.PlotsPages
{
    public partial class BarPage : BaseContentPage
    {
        public BarPage()
        {
            InitializeComponent();
            var data = new MockPlotData();
            var barChart = new BarChart() { Entries = data.DataPoints};
            
            this.chart.Chart = barChart;
        }
    }
}
