﻿using System;
using System.Collections.Generic;
using System.Globalization;
using PlotTest.Helpers;

namespace PlotTest.Service
{
    public class MockPlotData
    {

        public List<DataPoint> DataPoints;
        public MockPlotData()
        {
            DataPoints = new List<DataPoint>();
            Random rand = new Random();

            byte[] array = new byte[12 * 3];
            rand.NextBytes(array);
            for (int i = 0; i < 12; i++)
            {
                var value = (float)GetRandomNumber(-1000, 1000);

                string fullMonthName = "test";

                DataPoints.Add(
                    new DataPoint(value)
                    {
                        Label = fullMonthName,
                        Color = new SkiaSharp.SKColor(array[(i * 3)], array[(i * 3) + 1], array[(i * 3) + 2]),
                        ValueLabel = value.ToString()
                    });
            }
        }
        public double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
    }

}

