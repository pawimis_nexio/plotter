﻿using System;
using System.Threading.Tasks;
using PlotTest.ViewModel.Base;

namespace PlotTest.Service.Interface
{
    public interface INavService
    {
        Task PreviousPageAsync(bool initialise = true);

        Task PopAndReturnPages(int toRemove, bool initialise = true);

        Task BackToMainPageAsync();

        Task NavigateToViewModel<ViewModel, TParameter>(TParameter parameter, bool showNavBar) where ViewModel : BaseViewModel;

        Task NavigateToViewModel<ViewModel>(bool showNavBar) where ViewModel : BaseViewModel;

        Task NavigateToViewModelMasterDetail<ViewModelMaster,ViewModelDetail>(string masterTitle) where ViewModelMaster : BaseViewModel where ViewModelDetail : BaseViewModel;

        Task NavigateToViewModelTabbedPage<ViewModelTabbedPage>(BaseViewModel[] Children) where ViewModelTabbedPage : BaseViewModel;

        void ClearAllViewsFromStack();
    }
}
