﻿using System;
namespace PlotTest.Service.NavService
{
    public sealed class SingletonService<TService>
    {
        public TService Service { get; set; }
        private static SingletonService<TService> _instance;

        static internal SingletonService<TService> Instance()
        {
            if (_instance == null)
            {
                _instance = new SingletonService<TService>();
            }

            return _instance;
        }
    }
}
