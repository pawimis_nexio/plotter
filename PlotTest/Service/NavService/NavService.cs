﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using PlotTest.Service.Interface;
using PlotTest.ViewModel.Base;
using Xamarin.Forms;

namespace PlotTest.Service.NavService
{
    public class NavService : INavService
    {
        public INavigation navigation { get; set; }
        readonly IDictionary<Type, Type> _viewMapping = new Dictionary<Type, Type>();
        public void RegisterViewMapping(Type viewModel, Type view)
        {
            if (!_viewMapping.ContainsKey(viewModel))
                _viewMapping.Add(viewModel, view);
        }

        public async Task BackToMainPageAsync()
        {
            await navigation.PopToRootAsync(true);
        }

        public void ClearAllViewsFromStack()
        {
            if (navigation.NavigationStack.Count <= 1)
                return;

            for (var i = 0; i < navigation.NavigationStack.Count - 1; i++)
                navigation.RemovePage(navigation.NavigationStack[i]);
        }

        public async Task NavigateToViewModel<ViewModel, TParameter>(TParameter parameter, bool showNavBar) where ViewModel : BaseViewModel
        {
            Type viewType;
            if (_viewMapping.TryGetValue(typeof(ViewModel), out viewType))
            {
                var constructor = viewType.GetTypeInfo()
                    .DeclaredConstructors
                    .FirstOrDefault(dc => dc.GetParameters().Count() <= 0);

                var view = constructor.Invoke(null) as Page;
                var viewModel = (ViewModel)Activator.CreateInstance(typeof(ViewModel), null);
                view.BindingContext = viewModel;
                NavigationPage.SetHasNavigationBar(view, showNavBar);
                await navigation.PushAsync(view, true);
            }

            if (navigation.NavigationStack.Last().BindingContext is BaseViewModel<TParameter>)
                await ((BaseViewModel<TParameter>)(navigation.NavigationStack.Last().BindingContext)).InitAsync(parameter);
        }

        public async Task NavigateToViewModel<ViewModel>(bool showNavBar) where ViewModel : BaseViewModel
        {
            Type viewType;
            if (_viewMapping.TryGetValue(typeof(ViewModel), out viewType))
            {
                var constructor = viewType.GetTypeInfo()
                    .DeclaredConstructors
                    .FirstOrDefault(dc => dc.GetParameters().Count() <= 0);

                var view = constructor.Invoke(null) as Page;
                var viewModel = (ViewModel)Activator.CreateInstance(typeof(ViewModel), null);
                view.BindingContext = viewModel;
                NavigationPage.SetHasNavigationBar(view, showNavBar);
                await navigation.PushAsync(view, true);
            }

            if (navigation.NavigationStack.Last().BindingContext is BaseViewModel)
                await ((BaseViewModel)(navigation.NavigationStack.Last().BindingContext)).Init();
        }

        public async Task NavigateToViewModelMasterDetail<ViewModelMaster, ViewModelDetail>(string masterTitle) where ViewModelMaster : BaseViewModel where ViewModelDetail : BaseViewModel
        {
            Type viewTypeMaster, viewTypeDetail;
            if (_viewMapping.TryGetValue(typeof(ViewModelMaster), out viewTypeMaster)
                && _viewMapping.TryGetValue(typeof(ViewModelDetail),out viewTypeDetail))
            {
                var constructorMaster = viewTypeMaster.GetTypeInfo()
                    .DeclaredConstructors
                    .FirstOrDefault(dc => dc.GetParameters().Count() <= 0);

                var constructorDetail = viewTypeDetail.GetTypeInfo()
                    .DeclaredConstructors
                    .FirstOrDefault(dc => dc.GetParameters().Count() <= 0);

                var view = new MasterDetailPage();
                NavigationPage.SetHasNavigationBar(view, false);

                var viewMaster = constructorMaster.Invoke(null) as Page;
                viewMaster.Title = masterTitle;
                var viewModelMaster = (ViewModelMaster)Activator.CreateInstance(typeof(ViewModelMaster), null);
                viewMaster.BindingContext = viewModelMaster;

                var viewDetail = constructorDetail.Invoke(null) as Page;
                var viewModelDetail = (ViewModelDetail)Activator.CreateInstance(typeof(ViewModelDetail), null);
                viewDetail.BindingContext = viewModelDetail;

                view.Master = viewMaster;
                view.Detail = new NavigationPage(viewDetail);
                await navigation.PushAsync(view, true);
            }
           
        }
        public async Task NavigateToViewModelTabbedPage<ViewModelTabbedPage>(BaseViewModel[] Children) where ViewModelTabbedPage : BaseViewModel
        {
            Type viewType;
            if (_viewMapping.TryGetValue(typeof(ViewModelTabbedPage), out viewType))
            {
                var constructor = viewType.GetTypeInfo()
                    .DeclaredConstructors
                    .FirstOrDefault(dc => dc.GetParameters().Count() <= 0);

                var view = constructor.Invoke(null) as TabbedPage;
                var viewModel = (ViewModelTabbedPage)Activator.CreateInstance(typeof(ViewModelTabbedPage), null);
                view.BindingContext = viewModel;

                foreach(var item in Children)
                {
                    Debug.WriteLine(item.GetType());
                }


                await navigation.PushAsync(view, true);
            }

            if (navigation.NavigationStack.Last().BindingContext is BaseViewModel)
                await((BaseViewModel)(navigation.NavigationStack.Last().BindingContext)).Init();
        }

        public async Task PreviousPageAsync(bool initialise = true)
        {
            if (navigation.NavigationStack != null &&
                navigation.NavigationStack.Count > 1)
            {
                await navigation.PopAsync(true);
            }
            if (navigation.NavigationStack.Last().BindingContext is BaseViewModel && initialise)
                await ((BaseViewModel)(navigation.NavigationStack.Last().BindingContext)).Init();
        }

        public async Task PopAndReturnPages(int toRemove, bool initialise = true)
        {
            if (navigation.NavigationStack != null &&
                 navigation.NavigationStack.Count > toRemove + 1)
            {
                for (var counter = 1; counter < toRemove; counter++)
                {
                    navigation.RemovePage(navigation.NavigationStack[navigation.NavigationStack.Count - 2]);
                }
                await navigation.PopAsync(true);
            }
            if (navigation.NavigationStack.Last().BindingContext is BaseViewModel && initialise)
                await ((BaseViewModel)(navigation.NavigationStack.Last().BindingContext)).Init();
        }

        
    }
}
