﻿using System;
namespace PlotTest.Helpers
{
	public enum LineMode
	{
		None,
		Spline,
		Straight,
	}
}
