﻿using System;
namespace PlotTest.Helpers
{
    public enum PlotType
    {
        Bar,
        Donut,
        Line,
        Point,
        Radar,
        Radial
    }
}
