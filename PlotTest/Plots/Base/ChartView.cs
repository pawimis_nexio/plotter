﻿using System;
using SkiaSharp.Views.Forms;
using Xamarin.Forms;

namespace PlotTest.Plots.Base
{
	public class ChartView : SKCanvasView
	{
		public ChartView()
		{
			this.BackgroundColor = Color.Transparent;
			this.PaintSurface += OnPaintCanvas;
		}

		public static readonly BindableProperty ChartProperty = BindableProperty.Create(nameof(BaseChart), typeof(BaseChart), typeof(ChartView), null, propertyChanged: OnChartChanged);

		public BaseChart Chart
		{
			get { return (BaseChart)GetValue(ChartProperty); }
			set { SetValue(ChartProperty, value); }
		}

		private static void OnChartChanged(BindableObject bindable, object oldValue, object newValue)
		{
			((ChartView)bindable).InvalidateSurface();
		}

		private void OnPaintCanvas(object sender, SKPaintSurfaceEventArgs e)
		{
			if (this.Chart != null)
			{
				this.Chart.Draw(e.Surface.Canvas, e.Info.Width, e.Info.Height);
			}
		}
	}
}
