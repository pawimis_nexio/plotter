﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlotTest.Helpers;
using SkiaSharp;

namespace PlotTest.Plots.Base
{
    public  abstract class BaseChart
    {
        #region Properties

       
        public float Margin { get; set; } = 20;

       
        public float LabelTextSize { get; set; } = 16;

      
        public SKColor BackgroundColor { get; set; } = SKColors.White;

       
        public IEnumerable<DataPoint> Entries { get; set; }

       
        public float MinValue
        {
            get
            {
                if (!this.Entries.Any())
                {
                    return 0;
                }

                if (this.InternalMinValue == null)
                {
                    return Math.Min(0, this.Entries.Min(x => x.Value));
                }

                return Math.Min(this.InternalMinValue.Value, this.Entries.Min(x => x.Value));
            }

            set => this.InternalMinValue = value;
        }

     
        public float MaxValue
        {
            get
            {
                if (!this.Entries.Any())
                {
                    return 0;
                }

                if (this.InternalMaxValue == null)
                {
                    return Math.Max(0, this.Entries.Max(x => x.Value));
                }

                return Math.Max(this.InternalMaxValue.Value, this.Entries.Max(x => x.Value));
            }

            set => this.InternalMaxValue = value;
        }

        
        protected float? InternalMinValue { get; set; }

       
        protected float? InternalMaxValue { get; set; }

        #endregion

        #region Methods

       
        public void Draw(SKCanvas canvas, int width, int height)
        {
            canvas.Clear(this.BackgroundColor);

            this.DrawContent(canvas, width, height);
        }

        
        public abstract void DrawContent(SKCanvas canvas, int width, int height);

        
        protected void DrawCaptionElements(SKCanvas canvas, int width, int height, List<DataPoint> entries, bool isLeft)
        {
            var margin = 2 * this.Margin;
            var availableHeight = height - (2 * margin);
            var x = isLeft ? this.Margin : (width - this.Margin - this.LabelTextSize);
            var ySpace = (availableHeight - this.LabelTextSize) / ((entries.Count <= 1) ? 1 : entries.Count - 1);

            for (int i = 0; i < entries.Count; i++)
            {
                var entry = entries.ElementAt(i);
                var y = margin + (i * ySpace);
                if (entries.Count <= 1)
                {
                    y += (availableHeight - this.LabelTextSize) / 2;
                }

                var hasLabel = !string.IsNullOrEmpty(entry.Label);
                var hasValueLabel = !string.IsNullOrEmpty(entry.ValueLabel);

                if (hasLabel || hasValueLabel)
                {
                    var hasOffset = hasLabel && hasValueLabel;
                    var captionMargin = this.LabelTextSize * 0.60f;
                    var captionX = isLeft ? this.Margin : width - this.Margin - this.LabelTextSize;

                    using (var paint = new SKPaint
                    {
                        Style = SKPaintStyle.Fill,
                        Color = entry.Color,
                    })
                    {
                        var rect = SKRect.Create(captionX, y, this.LabelTextSize, this.LabelTextSize);
                        canvas.DrawRect(rect, paint);
                    }

                    if (isLeft)
                    {
                        captionX += this.LabelTextSize + captionMargin;
                    }
                    else
                    {
                        captionX -= captionMargin;
                    }

                    canvas.DrawCaptionLabels(entry.Label, entry.TextColor, entry.ValueLabel, entry.Color, this.LabelTextSize, new SKPoint(captionX, y + (this.LabelTextSize / 2)), isLeft ? SKTextAlign.Left : SKTextAlign.Right);
                }
            }
        }

        #endregion
    }
}
