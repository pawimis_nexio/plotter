﻿using System;
using PlotTest.Service.Interface;
using PlotTest.Service.NavService;
using PlotTest.View;
using PlotTest.View.PlotsPages;
using PlotTest.ViewModel;
using PlotTest.ViewModel.PlotsViewModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace PlotTest
{
    public partial class App : Application
    {
        SingletonService<INavService> singelton = SingletonService<INavService>.Instance();
        NavService navService;

        public App()
        {
            InitializeComponent();
            singelton.Service = navService = new NavService();
            navService.RegisterViewMapping(typeof(MainPageDetailModel), typeof(MainPageDetail));
            navService.RegisterViewMapping(typeof(MainPageMasterModel), typeof(MainPageMaster));
            navService.RegisterViewMapping(typeof(BarViewModel), typeof(BarPage));
            navService.RegisterViewMapping(typeof(DonutViewModel), typeof(DonutPage));
            navService.RegisterViewMapping(typeof(LineViewModel), typeof(LinePage));
            navService.RegisterViewMapping(typeof(PointViewModel), typeof(PointPage));
            navService.RegisterViewMapping(typeof(RadarViewModel), typeof(RadarPage));
            navService.RegisterViewMapping(typeof(RadialViewModel), typeof(RadialPage));


            NavigationPage mainPage = new NavigationPage();
            navService.navigation = mainPage.Navigation;
            Current.MainPage = mainPage;
            Start();
        }
        private async void Start()
        {
            await navService.NavigateToViewModelMasterDetail<MainPageMasterModel,MainPageDetailModel>("Plot Test");

        }
        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
