﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using PlotTest.Helpers;
using PlotTest.ViewModel.Base;
using PlotTest.ViewModel.PlotsViewModel;
using Xamarin.Forms;

namespace PlotTest.ViewModel
{
    public class MainPageDetailModel : BaseViewModel
    {
        ICommand _buttonClickedCommand;
        public ICommand ButtonClickedCommand => _buttonClickedCommand ?? new Command((param)=> ButtonClicked(param));

       

        public MainPageDetailModel()
        {
        }

        private async Task ButtonClicked(object param)
        {
            if (param == null) throw new Exception("Missing Main page navigation parameter");
            var navParam = (PlotType)param;
            switch(navParam){
                case PlotType.Bar:
                    await NavService.NavigateToViewModel<BarViewModel>(true);
                    break;
                case PlotType.Donut:
                    await NavService.NavigateToViewModel<DonutViewModel>(true);
                    break;
                case PlotType.Line:
                    await NavService.NavigateToViewModel<LineViewModel>(true);
                    break;
                case PlotType.Point:
                    await NavService.NavigateToViewModel<PointViewModel>(true);
                    break;
                case PlotType.Radar:
                    await NavService.NavigateToViewModel<RadarViewModel>(true);
                    break;
                case PlotType.Radial:
                    await NavService.NavigateToViewModel<RadialViewModel>(true);
                    break;
            }
        }
    }
}
