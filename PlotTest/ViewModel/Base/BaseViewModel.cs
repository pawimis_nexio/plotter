﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using PlotTest.Service.Interface;
using PlotTest.Service.NavService;

namespace PlotTest.ViewModel.Base
{
    public abstract class BaseViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        protected INavService NavService { get; private set; }

        protected BaseViewModel()
        {
            NavService = SingletonService<INavService>.Instance().Service;
        }



        bool isBusy;
        public bool Busy
        {
            get { return isBusy; }
            set
            {
                SetProperty(ref isBusy, value);
                Enable = !Busy;
            }
        }

        bool enable = true;
        public bool Enable
        {
            get => enable;
            set => SetProperty(ref enable, value);
        }

        protected bool SetProperty<T>(ref T backingStore,
                                        T value,
                                        [CallerMemberName]string propertyName = "",
                                        Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public virtual Task Init() { return null; }

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
    public abstract class BaseViewModel<Param> : BaseViewModel
    {
        protected BaseViewModel() : base()
        {
        }

        public override async Task Init()
        {
            await InitAsync(default);
        }

        public virtual Task InitAsync(Param param) { return null; }
    }
}
